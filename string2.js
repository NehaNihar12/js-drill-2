//return array of components of IPV4 addresses
function ipAddressComponents(testString){
    let reg = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;

    if(reg.test(testString)){
        let str = testString.match(reg)[0];
        let strarray = str.split('.')
        
        if(Number(strarray[0])>=0 && Number(strarray[0])<=255)
            return strarray
    }
    return []

}
module.exports =ipAddressComponents
