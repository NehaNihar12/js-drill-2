function convertStringToNumber(testString){
    let numberFormat =/(^[-$][$]{0,1}(\d{1,3}(,\d{3})*)+\.*\d*$)/;
    let regNum =/((\d{1,3}(,\d{3})*)+\.*\d*$)/
    if(numberFormat.test(testString)){
        let str = testString.match(regNum)[0]
        str=str.split(',').join('')
        return Number(str)
    }
    else
        return NaN
}
module.exports = convertStringToNumber;