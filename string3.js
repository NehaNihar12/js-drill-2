//check leap year
function isLeapYear(year){
    if(year%100!==0 && year%4===0){
        return true;
    }
    else if(year%400 ===0){
        return true
    }else{
        return false;
    }
}

//function print the month in which the date is present
function logTheMonth(date){

    //1.Gregorian calendar and was instituted in 1582. So year>=1582
    //2.date in the format MM/DD/YYYY
    
    let reg = /^([1-9]|0[1-9]|1[0-2])\/([1-9]|[1-2][0-9]|3[0-1])\/\d{4}$/;
    //test the input format
    if(reg.test(date)){

        let splitDate = date.split('/');

        let year = Number(splitDate[2]);
        let day = Number(splitDate[1])
        let month = Number(splitDate[0])

        //test the range of year
        if(year<1582){
            return 'Before 1582'
        }
        
        switch(month){
            case 1:
                return 'January'
            case 3:
                return 'March'  
            case 4:
                return 'April' 
            case 5:
                return 'May'  
            case 6:
                return 'June'  
            case 7:
                return 'July'
            case 8:
                return 'August'
            case 9:
                return 'September' 
            case 10:
                return 'October'  
            case 11:
                return 'November'   
            case 12:
                return 'December'  

        }

        if(month===2 && ( (isLeapYear(year) && day<=29) || (!isLeapYear(year) && day<=28) )){
            return 'February'
        }else{
            return 'Not a valid date in february';
        }
        
    }else{
        return `MM/DD/YYYY format input`
    }
}

module.exports = logTheMonth